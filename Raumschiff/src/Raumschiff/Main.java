package Raumschiff;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara","Borg-Schrott", 5);
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var", "Ferengi Schneckensaft", 200);
		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, 2, "IKS Heg'ta", "Forschungssonde", 35);
		
		klingonen.setLadungsVerzeichnis("Bat'leth", 200);
		
		romulaner.setLadungsVerzeichnis("Rote Materie", 2);
		romulaner.setLadungsVerzeichnis("Plasma", 50);
		
		vulkanier.setLadungsVerzeichnis("Photonentorpedo", 3);
		
		klingonen.photonenTorpedosSchiessen(romulaner, klingonen);
		romulaner.phaserkanoneSchiessen(klingonen, romulaner);
		
		vulkanier.nachricht("Gewalt ist nicht logisch");
		
		klingonen.doZustand();
		
		klingonen.photonenTorpedosSchiessen(romulaner, klingonen);
		klingonen.photonenTorpedosSchiessen(romulaner, klingonen);
		
		klingonen.doZustand();
		
		romulaner.doZustand();
		
		klingonen.setLadungsVerzeichnis("K�se", 50); 
		
		klingonen.doZustand();
		
		klingonen.getLadungsVerzeichnis();
	}

}
